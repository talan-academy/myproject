package com.talan.pfemanager.repositories;

import com.talan.pfemanager.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jive
 */
public interface RoleRepository extends JpaRepository<Role, Integer>{
    Role findByName(String name);
}
