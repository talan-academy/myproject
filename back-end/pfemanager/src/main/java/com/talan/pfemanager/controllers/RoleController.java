package com.talan.pfemanager.controllers;

import com.talan.pfemanager.entities.Role;
import com.talan.pfemanager.repositories.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jive
 */
@RestController
@ResponseBody
@RequestMapping(value="/roles")
public class RoleController {
    
    @Autowired
    private RoleRepository roleRepo;

    @GetMapping("")
    public List<Role> getRoles() {
        return roleRepo.findAll();
    }

    @GetMapping("/{id}")
    public Role getRole(@PathVariable int id) {
        return roleRepo.findById(id).get();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Role createRole(@RequestBody Role role) {
        return roleRepo.save(role);
    }

    @DeleteMapping("/{id}")
    public void deleteRole(@PathVariable int id) {
        roleRepo.deleteById(id);
    }

    @PutMapping("/{id}")
    public Role updateRole(@PathVariable int id, @RequestBody Role updatedRole) throws Exception{
        Role role = roleRepo.findById(id).get();
        if (role != null) {
            updatedRole.setId(id);
            System.out.println("updated");
            return roleRepo.save(updatedRole);
        }
        throw new Exception("could not find role id = " + id);
    }    

}
